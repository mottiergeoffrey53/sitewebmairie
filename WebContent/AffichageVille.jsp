<%@page import="java.util.ArrayList"%>
<%@page import="fr.esigelec.jee.Mairie"%>
<%@page import="fr.esigelec.jee.DAOsql"%>
<%@page import="fr.esigelec.jee.DAO_XML"%>
<%@page import="fr.esigelec.jee.Equipement"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<%
	DAOsql daosql = new DAOsql();
	
	String inseeCode = null;
	if(request.getParameter("insee") !=null){
		 inseeCode = request.getParameter("insee");
	}else{
		 response.sendRedirect("/MairieJavaJee/AffichageListe.jsp");
		 return;
		 //inseeCode="1001";
	}
	Mairie mairie = DAO_XML.Mairie(inseeCode);
	String nomDepartement = daosql.getNomDepartement(inseeCode);
	String nomMaire = daosql.getNomMaire(inseeCode);
	String prenomMaire = daosql.getPrenomMaire(inseeCode);
	ArrayList<Equipement> listeEquipements = daosql.getListeEquipements(inseeCode);
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Insert title here</title>

<!-- Font Awesome -->
<link rel="stylesheet"
	href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
<!-- Bootstrap core CSS -->
<link rel="stylesheet" href="MDBootstrap/css/bootstrap.min.css">
<!-- Material Design Bootstrap -->
<link rel="stylesheet" href="MDBootstrap/css/mdb.min.css">
<!-- MDBootstrap Datatables  -->
<link href="MDBootstrap/css/addons/datatables.min.css" rel="stylesheet">
</head>
<body>
	<div>
		<h1 class="row justify-content-md-center">Voici les d�tails de la
			ville de Rouen</h1>
		<div class="col-md-8 offset-md-2 border border-dark rounded">
			<div class="row justify-content-start">
				<span class="col-md-3">Commune :</span><span><%= mairie.getNom() %></span>
			</div>
			<div class="row">
				<span class="col-md-3">D�partement :</span><span> <%
 	out.print(nomDepartement);
 %>
				</span>
			</div>
			<div class="row">
				<span class="col-md-3">Code Postal :</span><span><%= mairie.getCodePostal() %></span>
			</div>
			<div class="row">
				<span class="col-md-3">Mairie :</span>
			</div>
			<div class="row">
				<span class="col-md-3 offset-1">Adresse :</span><span><%= mairie.getLigne() %></span>
			</div>
			<div class="row">
				<span class="col-md-3 offset-1">T�l�phone :</span><span><%= mairie.getTelephone() %></span>
			</div>
			<div class="row">
				<span class="col-md-3 offset-1">E-mail :</span><span><%= mairie.getEmail() %></span>
			</div>
			<div class="row">
				<span class="col-md-3 offset-1">Maire :</span><span> <%
 	out.print(nomMaire);
 %> <%
 	out.print(prenomMaire);
 %>
				</span>
			</div>
			<div class="row">
				<span class="col-md-3 offset-1">Horaires :</span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Lundi :</span><span><%= mairie.getOuverture()[0] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Mardi :</span><span><%= mairie.getOuverture()[1] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Mercredi :</span><span><%= mairie.getOuverture()[2] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Jeudi :</span><span><%= mairie.getOuverture()[3] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Vendredi :</span><span><%= mairie.getOuverture()[4] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Samedi :</span><span><%= mairie.getOuverture()[5] %></span>
			</div>
			<div class="row">
				<span class="col-md-2 offset-2">Dimanche :</span><span><%= mairie.getOuverture()[6] %></span>
			</div>
			<div class="row">
				<span class=" offset-1 mr-1">Sites web :</span><span><%= mairie.getUrl() %></span>
			</div>
		</div>
		<div class="col-md-8 offset-md-2">
			<table id="dtEquipements"
				class="table table-striped table-bordered table-sm" cellspacing="0"
				width="100%">
				<thead class="thead-dark">
					<tr>
						<th>Nom de l'�quipement</th>
						<th>Adresse</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					<%
						for (Equipement equipement : listeEquipements) {
							out.print("<tr><td>" + equipement.getNom() + "</td><td>" + equipement.getInsNoVoie() + " "
									+ equipement.getInsLibelleVoie() + "  " + equipement.getInsLieuDit() + "</td><td>"
							+ mairie.getCodePostal() + " " + mairie.getNom() + "</td></tr>");
						}
					%>
				</tbody>
			</table>
			<button class="col-lg-1 col-md-2 offset-10" onclick="location.href='AffichageListe.jsp'">Retour</button>
		</div>

	</div>
	<!-- jQuery -->
	<script type="text/javascript" src="MDBootstrap/js/jquery.min.js"></script>
	<!-- Bootstrap tooltips -->
	<script type="text/javascript" src="MDBootstrap/js/popper.min.js"></script>
	<!-- Bootstrap core JavaScript -->
	<script type="text/javascript" src="MDBootstrap/js/bootstrap.min.js"></script>
	<!-- MDB core JavaScript -->
	<script type="text/javascript" src="MDBootstrap/js/mdb.min.js"></script>
	<!-- MDBootstrap Datatables  -->
	<script type="text/javascript"
		src="MDBootstrap/js/addons/datatables.min.js"></script>
	<script>
		$(document).ready(function() {
			$('#dtEquipements').DataTable();
			$('.dataTables_length').addClass('bs-select');
		});
	</script>
</body>
</html>