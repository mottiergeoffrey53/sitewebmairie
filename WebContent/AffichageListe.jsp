<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="java.util.ArrayList"%>
<%@page import="fr.esigelec.jee.DAOsql"%>
<%@page import="fr.esigelec.jee.DAO_XML"%>
<%@page import="fr.esigelec.jee.Mairie"%>
<%@page import="fr.esigelec.jee.Equipement"%>  
<%
	
	DAOsql daosql = new DAOsql();
 	ArrayList<Mairie> mairies = new ArrayList<Mairie>();
 	String codePostal = null;
	if (request.getAttribute("mairies")!= null){
		 mairies = (ArrayList<Mairie>) request.getAttribute("mairies");
		 codePostal = mairies.get(0).getCodePostal();
	} 
	
%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Liste des communes de France</title>

<!-- MDB icon -->
  <link rel="icon" href="img/mdb-favicon.ico" type="image/x-icon">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.11.2/css/all.css">
  <!-- Google Fonts Roboto -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap">
  <!-- Bootstrap core CSS -->
  <link rel="stylesheet" href="MDBootstrap/css/bootstrap.min.css">
  <!-- Material Design Bootstrap -->
  <link rel="stylesheet" href="MDBootstrap/css/mdb.min.css">
  <!-- Your custom styles (optional) -->
  <link rel="stylesheet" href="css/style.css">
  <!-- MDBootstrap Datatables  -->
  <link href="MDBootstrap/css/addons/datatables.min.css" rel="stylesheet">

<style type="text/css"> 

    .btn-circle.btn-sm { 
        width: 30px; 
        height: 30px; 
        padding: 6px 0px; 
        border-radius: 15px; 
        font-size: 8px; 
        text-align: center; 
    }
    
</style>

</head>
<!--Main Navigation-->
<header>
<div class="container-fluid" style="padding: 20px">

	<div class="d-flex flex-sm-column justify-content-sm-center ">
		
		<% if(codePostal==null){ %>
			
			<h1 class="ml-5 mt-3 align-self-center">Entrez un code postal � chercher</h1>
		
		<% } else { %>
			<h1 class="ml-5 mt-3 align-self-center">Voici la liste des communes de France</h1>
			<h2 class="ml-5 mt-3 align-self-center">Pour le code postal : <%=codePostal%></h2>
		
		<% } %>
	</div>
	
	<div class="d-flex flex-sm-row justify-content-sm-end mr-3 mt-7">
		<form action="/MairieJavaJee/ControllerListe" method="post">
			<div class="col-sm form-group mt-5">
				<label for="cp">Code Postal:</label> <br> 
				<input type="text" id="cp" name="cp" maxlength="5">
			</div>
		</form>	
	</div>
	
</div>
</header>


	<main>
		<div class="container-fluid p-5">
			<div class="row pt-5">
				<div class="col text-center">
					<table id="dtBasicExample"
						class="table table-striped table-bordered btn-table" cellspacing="0"
						width="100%">
						<thead>
							<tr>
								<th class="th-sm">Nom</th>
								<th class="th-sm">D�partement</th>
								<th class="th-sm">Code Postal</th>
								<th class="th-sm">Nombre d'�quipements</th>
								<th class="th-sm" width="5%"></th>
							</tr>
						</thead>
						<tbody>
						<%
						
						for (Mairie mairie : mairies) {
							
						%>	
						
							<tr>
								<td><%= mairie.getNom() %></td>
								<td><%= mairie.getDepartement() %></td>
								<td><%= mairie.getCodePostal() %></td>
								<td><%= mairie.getNbEquipements() %></td>
								<td><a href="/MairieJavaJee/AffichageVille.jsp?insee=<%=mairie.getCodeInsee()%>" class="btn btn-circle btn-sm m-0" role="button"><i class="fas fa-search mt-1"></i></a></td>
							</tr>
						
						<%
						}
						
						%>
							
						</tbody>
						
					</table>

					<!-- jQuery -->
					<script type="text/javascript" src="MDBootstrap/js/jquery.min.js"></script>
					<!-- Bootstrap tooltips -->
					<script type="text/javascript" src="MDBootstrap/js/popper.min.js"></script>
					<!-- Bootstrap core JavaScript -->
					<script type="text/javascript" src="MDBootstrap/js/bootstrap.min.js"></script>
					<!-- MDB core JavaScript -->
					<script type="text/javascript" src="MDBootstrap/js/mdb.min.js"></script>
					<!-- Your custom scripts (optional) -->
					<script type="text/javascript"></script>
					<!-- MDBootstrap Datatables  -->
					<script type="text/javascript" src="MDBootstrap/js/addons/datatables.min.js"></script>

					<script>
						$(document).ready(function () {
						$('#dtBasicExample').DataTable();
						$('.dataTables_length').addClass('bs-select');
						});
					</script>
				</div>
			</div>
		</div>
	</main>


</html>