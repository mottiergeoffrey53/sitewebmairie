package fr.esigelec.jee;

public class Equipement {

	private String nom;
	private String InsNoVoie;
	private String InsLibelleVoie;
	private String InsLieuDit;
	
	public Equipement(String nom, String InsNoVoie, String InsLibelleVoie, String InsLieuDit) {
		this.nom = nom;
		this.InsNoVoie = InsNoVoie;
		this.InsLibelleVoie = InsLibelleVoie;
		this.InsLieuDit = InsLieuDit;
	}
	
	public String toString() {
		return new String("\nnom : " + nom + "\nInsNoVoie : " + InsNoVoie + "\nInsLibelleVoie : " + InsLibelleVoie + "\nInsLieuDit : " + InsLieuDit + "\n");
	}
	
	public String getNom() {
		return nom;
	}

	public String getInsNoVoie() {
		return InsNoVoie;
	}

	public String getInsLibelleVoie() {
		return InsLibelleVoie;
	}

	public String getInsLieuDit() {
		return InsLieuDit;
	}
}
