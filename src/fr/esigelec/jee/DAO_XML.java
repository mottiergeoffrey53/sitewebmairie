package fr.esigelec.jee;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import org.jdom2.*;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.w3c.dom.Node;

public class DAO_XML {
	
	static org.jdom2.Document document;
	static Element racine;
	
	
	/**
	 * Retourne l'objet mairie selon le code insee
	 * 
	 * @param code
	 * @return objet Mairie
	 */
	public static Mairie Mairie(String codeInsee) {

		File repertoire = new File("D:\\Documents\\Ecole\\ISN\\JEE\\Projet\\Projet 2\\all_20200309\\organismes\\" + codeInsee.substring(0,2));
		String liste[] = repertoire.list();      
		String r = "";
		
		//On cr�e une instance de SAXBuilder
		SAXBuilder sxb = new SAXBuilder();
		
		if (liste != null) {         
			for (int i = 0; i < liste.length; i++) {
				if (liste[i].substring(0, 6).equals("mairie") && liste[i].substring(7, 12).equals(codeInsee)) {
					//System.out.println(liste[i]);
					r = "D:\\Documents\\Ecole\\ISN\\JEE\\Projet\\Projet 2\\all_20200309\\organismes\\" + codeInsee.substring(0,2) + "\\" + liste[i];
				}
			}
			
		} else {
			System.err.println("Nom de repertoire invalide");
			r = "erreur";
		}

		/////////parse

		try
		{
			//On cr�e un nouveau document JDOM avec en argument le fichier XML
			//Le parsing est termin� 
			document = sxb.build(new File(r));
		}
		catch(Exception e){
			e.printStackTrace();
		}

		//On initialise un nouvel �l�ment racine avec l'�l�ment racine du document.
		racine = document.getRootElement();
		
		//on cr�� l'objet mairie avec comme param�tres les infos r�cup�r�es avec la methode getInfo
		Mairie mairie = new Mairie(getInfo("Insee"), getInfo("Nom"), getInfo("Adresse"), getInfo("Code Postal"), getInfo("Telephone"), getInfo("Email"), getInfo("Url"));
		mairie.setOuverture(getOuverture());
		
		return mairie;
		
	}
	
	
	/**
	 * Permet de parser le fichier xml pour r�cup�rer les infos d'une mairie
	 * @param choix info que l'on souhaite r�cuperer
	 * @return String qui correspond � l'info cherch�e
	 */
	public static String getInfo(String choix)
	{
		String r = null;
		//Instanciation des Elements JDOM
		Element Nom = racine.getChild("Nom");
		Element Adresse = racine.getChild("Adresse");
		Element ligne = Adresse.getChild("Ligne");
		Element CodePostal = Adresse.getChild("CodePostal");
		
		Element Coordonn�esNum = racine.getChild("Coordonn�esNum");
		Element Url;
		Element Tel;
		Element Email;
		
		//try catchs au cas ou l'info n'existe pas dans le fichier xml
		try {
			Url = Coordonn�esNum.getChild("Url");
		} catch (Exception e) {
			// TODO: handle exception
			Url = null;
		}
		try {
			Tel = Coordonn�esNum.getChild("T�l�phone");
		} catch (Exception e) {
			// TODO: handle exception
			Tel = null;
		}
		try {
			Email = Coordonn�esNum.getChild("Email");
		} catch (Exception e) {
			// TODO: handle exception
			Email = null;
		}

		switch (choix) {
			case "Nom": {
				if (Nom != null) {
					r = Nom.getText().substring(9);
				}else {
					r="";
				}
				
				break;
			}
			case "Adresse": {
				if (ligne != null) {
					r = ligne.getText();
				}else {
					r="";
				}
				
				break;
			}
			case "Insee": {
				
				r = racine.getAttribute("codeInsee").getValue();
				break;
			}
			case "Code Postal": {
				if (CodePostal != null) {
					r = CodePostal.getText();
				}else {
					r="";
				}
				
				break;
			}
			case "Url": {
				if (Url != null) {
					r = Url.getText();
				}else {
					r="";
				}
				
				break;
			}
			case "Telephone": {
				
				if (Tel != null) {
					r = Tel.getText();
				}else {
					r="";
				}
				
				break;
			}
			case "Email": {
				if (Email != null) {
					r = Email.getText();
				}else {
					r="";
				}
				
				break;
			}

		}
	   
	   return r;
	}
	
	/**
	 * Permet d'avoir les horaires d'ouverture d'une mairie
	 * @return un tableau de String contenant les horaires d'ouvertures
	 */
	public static String[] getOuverture(){

		//Hashmap pour it�rer parmis les jours de la semaine. Exemple d�but: lundi fin: mercredi 
		//Pour pouvoir remplir la case du tableau de mardi on fait pour i = lundi (1) to mercredi(3) i++
		//Ainsi on remplis les infos pour mardi(2) �galement
		HashMap<String, Integer> joursSemaine = new HashMap<String, Integer>();
		joursSemaine.put("lundi", 1);
		joursSemaine.put("mardi", 2);
		joursSemaine.put("mercredi", 3);
		joursSemaine.put("jeudi", 4);
		joursSemaine.put("vendredi", 5);
		joursSemaine.put("samedi", 6);
		joursSemaine.put("dimanche", 7);

		try {
			Element Ouverture = racine.getChild("Ouverture");
			List plageJListe = Ouverture.getChildren("PlageJ");

			String semaine[] = {"","","","","","",""};

			Iterator i = plageJListe.iterator();

			while(i.hasNext())
			{
				Element courantJour = (Element)i.next();
				//On affiche le nom de l'�l�ment courant

				int dernierJour = joursSemaine.get(courantJour.getAttribute("fin").getValue());
				int premierJour = joursSemaine.get(courantJour.getAttribute("d�but").getValue());


				List plageHListe = courantJour.getChildren("PlageH");
				Iterator j = plageHListe.iterator();
				while(j.hasNext())
				{
					Element courantHoraire = (Element)j.next();

					String horaireFin = courantHoraire.getAttribute("fin").getValue();
					String horaireDebut = courantHoraire.getAttribute("d�but").getValue();

					for (int k = premierJour; k <= dernierJour; k++) {

						semaine[k-1] = semaine[k-1] + horaireDebut + " - " + horaireFin + "  ";

					}
				}
			}	

			return semaine;

		} catch (Exception e) {
			return null;
		}

	}
	
	/**
	 * retourne tout les insee des mairies aillant le m�me code postal
	 * @param codeP code postal
	 * @return liste des insee des mairies aillant le m�me code postal
	 */
	public static ArrayList<String> getInsee(String codeP){
		ArrayList<String> codesInsee = new ArrayList<String>();
		String listeMairies[] = null;

		// chemin relatif : \\wtpwebapps\\sitewebmairie\\organismes\\
		// organismes dans webcontent
		File mairies = new File("D:\\Documents\\Ecole\\ISN\\JEE\\Projet\\Projet 2\\all_20200309\\organismes\\" + codeP.substring(0,2));
		listeMairies = mairies.list();

		if (listeMairies != null) {         
			for (int j = 0; j < listeMairies.length; j++) {

				//si le code postal de la mairie = l'argument de la methode
				if (Mairie(listeMairies[j].substring(7,12)).getCodePostal().equals(codeP)) {
					//on ajoute le code insee de la mairie � la liste
					codesInsee.add(listeMairies[j].substring(7,12));
				} 

			}

		} else {
			System.err.println("Nom de repertoire invalide");
		}

		return codesInsee;
	}

}

