package fr.esigelec.jee;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class ConfigAttributes {
	
	public ConfigAttributes() {
		
	}
	
	public String getItemPropertie(String propertieName) {
		String propertieValue = null;
		InputStream inputStream = null;
		
		try {
			Properties properties = new Properties();
			String propFileName = "config.properties";
 
			inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
 
			properties.load(inputStream);

			propertieValue = properties.getProperty(propertieName);

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return propertieValue;
	}
}
