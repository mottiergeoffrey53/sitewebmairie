package fr.esigelec.jee;

public class Mairie {
	
private String codeInsee;
private String ligne;
private String codePostal;
private String telephone;
private String email;
private String url;
private String[] semaine;
private String nom;
private String departement;
private int nbEquipements;




public Mairie(String codeInsee, String nom, String ligne, String codePostal, String telephone, String email, String url) {
	super();
	
	this.codeInsee = codeInsee;
	this.ligne = ligne;
	this.codePostal = codePostal;
	this.telephone = telephone;
	this.email = email;
	this.url = url;
	this.nom = nom;
	this.semaine = new String[7];
}

public String toString() {
	
	return 	  "Nom :" + nom + "\n"
			+ "Adresse :" + ligne + "\n" 
			+ "Code Postal:" + codePostal + "\n\n"
			+ "Code Insee:" + codeInsee + "\n"
			+ "Telephone:" + telephone + "\n"
			+ "Email:" + email + "\n"
			+ "Url:" + url + "\n\n"
			+ "Ouverture:\nLundi:" + semaine[0] + "\n"
			+ "Mardi:" + semaine[1] + "\n"
			+ "Mercredi:" + semaine[2] + "\n"
			+ "Jeudi:" + semaine[3] + "\n"
			+ "Vendredi:" + semaine[4] + "\n"
			+ "Samedi:" + semaine[5] + "\n"
			+ "Dimanche::" + semaine[6] + "\n";
}

public String getCodeInsee() {
	return codeInsee;
}

public void setCodeInsee(String codeInsee) {
	this.codeInsee = codeInsee;
}

public String getLigne() {
	return ligne;
}

public void setLigne(String ligne) {
	this.ligne = ligne;
}

public String getCodePostal() {
	return codePostal;
}

public void setCodePostal(String codePostal) {
	this.codePostal = codePostal;
}

public String getTelephone() {
	return telephone;
}

public void setTelephone(String telephone) {
	this.telephone = telephone;
}

public String getEmail() {
	return email;
}

public void setEmail(String email) {
	this.email = email;
}

public String getUrl() {
	return url;
}

public void setUrl(String url) {
	this.url = url;
}

public String[] getOuverture() {
	return semaine;
}

public void setOuverture(String[] ouverture) {
	this.semaine = ouverture;
}

public String getNom() {
	return nom;
}

public void setNom(String nom) {
	this.nom = nom;
}
public String getDepartement() {
	return departement;
}

public void setDepartement(String departement) {
	this.departement = departement;
}

public int getNbEquipements() {
	return nbEquipements;
}

public void setNbEquipements(int nbEquipements) {
	this.nbEquipements = nbEquipements;
}
	
}
