package fr.esigelec.jee;

import java.io.IOException;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class ControllerListe
 */
@WebServlet("/ControllerListe")
public class ControllerListe extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ControllerListe() {
        super();
        // TODO Auto-generated constructor stub
    }


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		DAOsql daosql = new DAOsql();
		String codeP = null;
		ArrayList<Mairie> mairies = new ArrayList<Mairie>();
		RequestDispatcher requestDispatcher = request.getRequestDispatcher("/AffichageListe.jsp");
		
		//parametre cp non nul si appel� depuis l'input code postal
		if(request.getParameter("cp")!=null){
			codeP = request.getParameter("cp");
		}
		
		//pour toutes les mairies ayant le m�me code postal
		for (String insee : DAO_XML.getInsee(codeP)) {
			//System.out.println(insee);
			
			//Cr�ation de l'objet mairie
			Mairie mairie = DAO_XML.Mairie(insee);
			mairie.setDepartement(daosql.getNomDepartement(insee));
			mairie.setNbEquipements(daosql.getListeEquipements(insee).size());
			//ajout de la mairie � la liste
			mairies.add(mairie);
			
		}
		//ajout de la liste � l'attribut qui doit �tre envoy�
		request.setAttribute("mairies", mairies);
		requestDispatcher.forward(request, response);
	}

}
