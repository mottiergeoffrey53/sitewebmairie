package fr.esigelec.jee;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class DAOsql {

	static String URL;
	static String LOGIN;
	static String PASS;;
	Connection conn = null;

	static {
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}

	public DAOsql() {

	}

	public boolean connect() {
		ConfigAttributes config = new ConfigAttributes();
		LOGIN = config.getItemPropertie("daoLogin");
		PASS = config.getItemPropertie("daoPassword");
		URL = config.getItemPropertie("daoURL");
		try {
			conn = DriverManager.getConnection(URL, LOGIN, PASS);
		} catch (SQLException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}

	public void close() {
		if (conn != null) {
			try {
				conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
	}

	// retourne tout les insee du departement
	public ArrayList<String> getInsee(String dept) {
		ArrayList<String> codes = new ArrayList<String>();

		if (connect()) {
			String query = "SELECT DISTINCT ComInsee FROM installation WHERE DepLib =?";
			try {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, dept);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					codes.add(rs.getString(1));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
		}
		return codes;
	}

	public String getNomDepartement(String InseeCode) {
		String nomDepartement = null;
		if (connect()) {
			String query = "SELECT DepLib FROM installation WHERE ComInsee = ?;";
			try {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, InseeCode);
				ResultSet rs = ps.executeQuery();
				rs.next();
				nomDepartement = rs.getString("DepLib");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
		}
		return nomDepartement;
	}

	public String getNomMaire(String InseeCode) {
		String nomMaire = null;
		if (connect()) {
			String query = "SELECT nom FROM maire WHERE CodeInsee = ?;";
			try {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, InseeCode);
				ResultSet rs = ps.executeQuery();
				rs.next();
				nomMaire = rs.getString("nom");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
		}
		return nomMaire;
	}

	public String getPrenomMaire(String InseeCode) {
		String prenomMaire = null;
		if (connect()) {
			String query = "SELECT prenom FROM maire WHERE CodeInsee = ?;";
			try {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, InseeCode);
				ResultSet rs = ps.executeQuery();
				rs.next();
				prenomMaire = rs.getString("prenom");
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
		}
		return prenomMaire;
	}

	public ArrayList<Equipement> getListeEquipements(String InseeCode) {
		ArrayList<Equipement> listeEquipements = new ArrayList<Equipement>();
		if (connect()) {
			String query = "SELECT * FROM jee.equipement right JOIN jee.installation on equipement.InsNumeroInstall = installation.InsNumeroInstall where equipement.ComInsee = ?;";
			try {
				PreparedStatement ps = conn.prepareStatement(query);
				ps.setString(1, InseeCode);
				ResultSet rs = ps.executeQuery();
				while (rs.next()) {
					listeEquipements.add(new Equipement(rs.getString("EquNom"), rs.getString("InsNoVoie"),
							rs.getString("InsLibelleVoie"), rs.getString("InsLieuDit")));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			} finally {
				close();
			}
		}
		return listeEquipements;
	}

}
