package fr.esigelec.jee;

public class Main {

	public static void main(String[] args) {
		
		long debut = System.currentTimeMillis();
		
		DAOsql daosql = new DAOsql();
		System.out.println(daosql.getNomDepartement("76540"));
		System.out.println(daosql.getNomMaire("76540"));
		System.out.println(daosql.getPrenomMaire("76540"));
		System.out.println(daosql.getListeEquipements("76540"));
		
		long fin = System.currentTimeMillis();
		System.out.println("M�thode ex�cut�e en " + Long.toString(fin - debut) + " millisecondes");
	}

}
